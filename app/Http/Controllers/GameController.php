<?php

namespace App\Http\Controllers;

use App\board;
use App\board_pices;
use App\game;
use App\move;
use App\pices;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class GameController extends Controller
{
    public function index(Request $request)
    {
        $id = [];
        $x = [];
        $y = [];
        $row = $request->input("row");
        $column = $request->input("column");
        $pieces = $request->input("pieces");
        $command = $request->input("command");
        if ($request->has("newGame")) {
            foreach ($pieces as $piece) {
                $piece = pices::firstOrCreate(['x' => $piece['x'], 'y' => $piece['y']]);
            }
        }
        foreach ($pieces as $piece) {
            $enterPices = pices::where("x", $piece["x"])->where("y", $piece["y"])->first();

            $id[] = $enterPices->id;
            $x[] = $enterPices->x;
            $y[] = $enterPices->y;


            $enterPicesCommand[] = $piece["command"];
        }
        $makeGame = game::create(["isRunning" => 1]);
        $currentId = Game::orderBy("id", "desc")->take(1)->first();
        foreach (Game::cursor() as $game) {
            $game::where("id", "!=", $currentId->id)->update(["isRunning" => 0]);
        }
        $board = board::create(array(
            'rows' => $row,
            'column' => $column,
            'gameId' => $currentId->id

        ));
        $lastBoardId = 0;
        $lastBoard = board::orderBy("id", "desc")->take(1)->first();
        $lastBoardId = $lastBoard->id;
        for ($i = 0; $i < sizeof($id); $i++) {
            board_pices::create([
                "boardId" => $lastBoardId,
                "picesId" => $id[$i],
                "x" => $x[$i],
                "y" => $y[$i],
                "commands" => $enterPicesCommand[$i],
                "status" => 1
            ]);
            $lastBoardPiceId = board_pices::orderBy("id", "desc")->take(1)->first();
        }
        $newPieces = board_pices::where('boardId', $lastBoardId)->get();
        return View("play", [
            "row" => $row,
            "column" => $column,
            "noOfPices" => sizeof($pieces),
            "pieces" => $newPieces,
            "boardId" => $lastBoardId,
            "picesIds" => $id
        ]);
    }
    public function move(Request $request)
    {
        $error = [];
        $refershFlag = false;
        $user = Auth::user();
        //dd($user);
        $currentGame = $user->game()->where("isRunning", 1)->first();
        if ($currentGame == null) {
            return redirect("/game");
        }
        $last_board = board::where("id", $currentGame->id)->first();
        $newPieces = board_pices::where('boardId', $last_board->id)->get();
        // dd($newPieces);
        $flag = false;
        $invalidMove = false;
        $row = $last_board->rows;
        $column = $last_board->column;
        $gameid = $last_board->gameId;
        foreach ($newPieces as $piece) {
            $oldValue["x"] = $piece->x;
            $oldValue["y"] = $piece->y;
            $makeOldValues[] = $oldValue;
        }
        $ismove = 0;
        foreach ($newPieces as $piece) {
            $pieceMoves = explode(",", $piece->commands);
            $pieceMove = array_shift($pieceMoves);
            if ($pieceMove) {
                $ismove++;
            }
          //  dump($piece);

            if (sizeof($pieceMoves) == 1) {
                board_pices::where("id", $piece->id)->update(["status" => 0]);
            }
            if ($pieceMove == 'up') {
                if (!($piece->x == 1)) {
                    $piece->x -= 1;
                    $newCommand = implode(",", $pieceMoves);
                    board_pices::where('id', $piece->id)->where('boardId', $last_board->id)
                        ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => $newCommand]);
                    move::create(["boardId" => $last_board->id, "picesId" => $piece->id, "commands" => $pieceMove]);

                } else {
                    board_pices::where('id', $piece->id)->where('boardId', $last_board->id)
                        ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => implode(",", $pieceMoves)]);
                    //$error[] = "can'y up";
                    $request->session()->flash("error", "can not up");
                    break;
                }
            } else {
                if ($pieceMove == 'down') {
                    //dd("false");
                    if ($piece->x == $last_board->rows) {
                        board_pices::where('id', $piece->id)->where('boardId', $last_board->id)
                            ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => implode(",", $pieceMoves)]);
                        //  echo '<script language="javascript"> alert("can not move down") </script>';
                       // $error[] = "can't down";
                        $request->session()->flash("error", "can not down");
                        break;
                    }
                    $piece->x += 1;
                    board_pices::where('id', $piece->id)->where('boardId', $last_board->id)
                        ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => implode(',', $pieceMoves)]);
                    move::create(["boardId" => $last_board->id, "picesId" => $piece->id, "commands" => $pieceMove]);


                } else {
                    if ($pieceMove == "left") {
                        if ($piece->y == 1) {

                            board_pices::where('id', $piece->id)->where('boardId', $last_board->id)
                                ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => implode(",", $pieceMoves)]);

                            // echo '<script language="javascript"> alert("can not move left") </script>';
                            //$error[] = "can't left";
                            $request->session()->flash("error", "can not left");


                            break;
                        }
                        $piece->y -= 1;
                        board_pices::where('id', $piece->id)->where('boardId', $last_board->id)
                            ->update(['x' => $piece->x, 'y' => $piece->y, 'commands' => implode(',', $pieceMoves)]);
                        move::create(["boardId" => $last_board->id, "picesId" => $piece->id, "commands" => $pieceMove]);


                    } else {
                        if ($pieceMove == "right") {
                            if (!($piece->y == $last_board->columns)) {
                                $piece->y += 1;
                                board_pices::where('id', $piece->id)->where('boardId', $last_board->id)
                                    ->update([
                                        'x' => $piece->x,
                                        'y' => $piece->y,
                                        'commands' => implode(',', $pieceMoves)
                                    ]);
                                move::create([
                                    "boardId" => $last_board->id,
                                    "picesId" => $piece->id,
                                    "commands" => $pieceMove
                                ]);


                            } else {
                                board_pices::where('id', $piece->id)->where('boardId', $last_board->id)
                                    ->update([
                                        'x' => $piece->x,
                                        'y' => $piece->y,
                                        'commands' => implode(",", $pieceMoves)
                                    ]);
                                // echo '<script language="javascript"> alert("can not move right") </script>';
                               // $error[] = "can't right";
                                $request->session()->flash("error", "can not right");
                                break;
                            }

                        } else {
                            $invalidMove = true;
                        }
                    }
                }
            }


        }
        /*foreach ($makeOldValues as $makeOldValue) {
            if ($makeOldValue["x"] == $piece->x && $makeOldValue["y"] == $piece->y) {
                $error[] = "conflict";
                break;

            }

        }*/
        $newPieces = board_pices::where('boardId', $last_board->id)->get();
        if ($ismove != 0) {
            echo " <script>
  setTimeout(function(){location.reload() }, 3000);
      </script>";
        } else {
            $request->session()->flash("error", "game over");
            game::where("id", $currentGame->id)->update(["isRunning" => 0]);

        }

        return view('play',
            ['row' => $row, 'column' => $column, 'gameid' => $gameid, 'pieces' => $newPieces])->with("errors", $error);
    }


}
