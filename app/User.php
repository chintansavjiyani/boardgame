<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','verification_code','verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function sendVerificationEmail()
    {

//optionally check if the user has a verification code here

        Mail::send('emails.userverification',
            ['verification_code' => $this->verification_code],
            function ($message) {
                $message->to($this->email)
                    ->subject('Please verify your email');
                return true;
            });

    }
    public function game()
    {
        return $this->hasMany('App\game');
    }
}
