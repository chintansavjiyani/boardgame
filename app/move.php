<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class move extends Model
{
    protected $table='move';
    protected $fillable=["boardId","picesId","commands"];
    protected $softDelete = true;




    public function board()
    {
        return $this->hasMany('App\board');
    }

    public function pices()
    {
        return $this->hasMany('App\pices');
    }




}
