<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pices extends Model
{
    protected $table='pices';
    protected $fillable=["id","x","y"];

    public function board_pices()
    {
        return $this->belongsTo('App\board_pices');
    }
    public function board()
    {
        return $this->belongsTo('App\move');
    }
}
