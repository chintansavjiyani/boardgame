<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class board extends Model
{
    protected $table='board';
    protected $fillable=["id","rows","column","gameId"];

    public function game()
    {
        return $this->belongsTo('App\game');
    }
    public function board_pices()
    {
        return $this->belongsTo('App\board_pices');
    }
    public function board()
    {
        return $this->belongsTo('App\move');
    }

}
