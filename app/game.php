<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class game extends Model
{
    protected $table = 'game';
    protected $fillable = ["id", "isRunning","user_id"];


    public function board()
    {
        return $this->hasOne('App\board');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
