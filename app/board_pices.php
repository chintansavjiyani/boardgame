<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class board_pices extends Model
{
    protected $table='board_pices';
    protected $fillable=["id","boardId","picesId","x","y","commands","status"];
    public function board()
    {
        return $this->hasMany('App\board');
    }
    public function pices()
    {
        return $this->hasMany('App\pices');
    }
}
