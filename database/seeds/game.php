<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class game extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user= \App\User::first();
     // $user=  Auth::user();
    //  dd($user->id);
        factory(App\game::class, 2)->create(['user_id' => $user->id]);
    }
}
