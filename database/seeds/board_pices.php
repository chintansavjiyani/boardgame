<?php

use Illuminate\Database\Seeder;

class board_pices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $boards=\App\board::all();
        foreach ($boards as $board)
        {
           $noOfPices= rand(1,4);
           for($i=1;$i<=$noOfPices;$i++) {
               $picesX = rand(1, $board->rows);
               $picesY = rand(1, $board->column);
               $pices = \App\pices::firstOrCreate(["x" => $picesX, "y" => $picesY]);

               // dump($pices);

               factory(App\board_pices::class)->create([
                   "boardId" => $board->id,
                   "picesId" => $pices->id,
                   "x" => $picesX,
                   "y" => $picesY
               ]);
           }

        }










    }
}
