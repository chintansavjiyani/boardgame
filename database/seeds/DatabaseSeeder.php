<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    protected $boardRow;
    protected $boardColumn;
    protected $boardId;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('user');
        $this->call("game");
        $this->call("board");
        $this->call("board_pices");
    }
}


