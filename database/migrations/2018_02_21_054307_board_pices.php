<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BoardPices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_pices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("boardId")->unsigned();
            // $table->foreign("boardId")->references('id')->on('game');
            $table->integer("picesId")->unsigned();
            //$table->integer("picesId")->references('id')->on('pices');
            $table->integer("x");
            $table->integer("y");
            $table->string("commands");
            $table->integer("status");

            $table->timestamps();
        });
        Schema::table('board_pices', function (Blueprint $table) {
            //$table->integer('boardId')->unsigned();

            $table->foreign('boardId')->references('id')->on('board');


            $table->foreign('picesId')->references('id')->on('pices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
