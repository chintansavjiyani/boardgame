<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Board extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rows');
            $table->integer('column');
            $table->integer('gameId')->unsigned();
            // $table->foreign('gameId')->references('id')->on('game');
            $table->timestamps();


        });
      Schema::table('board', function (Blueprint $table) {
            // $table->integer('gameId')->unsigned();

             $table->foreign('gameId')->references('id')->on('game');
         });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
