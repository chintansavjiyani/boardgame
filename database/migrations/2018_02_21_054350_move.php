<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Move extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('move', function (Blueprint $table) {
            $table->increments('id');

            $table->integer("boardId")->unsigned();
            //$table->foreign("boardId")->references('id')->on('board');
            $table->integer("picesId")->unsigned();
            //$table->integer("picesId")->references('id')->on('pices');
            $table->string("commands");
            $table->timestamps();
            $table->softDeletes();
        });
         Schema::table('move', function (Blueprint $table) {

              $table->foreign('boardId')->references('id')->on('board');
             $table->foreign('picesId')->references('id')->on('pices');
          });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
